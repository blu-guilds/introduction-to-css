import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  getUsersUrl = 'https://api.github.com/users?page=0&per_page=1000';
  error: boolean;

  constructor(
    private http: HttpClient,
  ) { }

  getUsers(): Observable<any[]> {
    this.error = false;
    return this.http.get<any[]>(this.getUsersUrl).pipe(
      tap(() => this.log('users loaded succesfully')),
      catchError(this.handleError('getUsers', []))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead of writing a log
      this.log(`${operation} failed: ${error.message}`);
      this.error = true;
      return of(result as T);
    };
  }

}
