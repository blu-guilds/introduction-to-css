import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  mobile: boolean;
  showMenu: boolean;

  ngOnInit() {
    this.resize();
    window.addEventListener('resize', this.resize, true);
  }
  ngOnDestroy() {
    window.removeEventListener('resize', this.resize, true);
  }
  resize() {
    this.mobile = window.innerWidth <= 575 ? true : false;
    this.showMenu = window.innerWidth > 575 ? true : false;
  }

  menuToggle() {
    this.showMenu = !this.showMenu;
  }
}
