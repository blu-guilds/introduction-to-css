import { Component, OnInit } from '@angular/core';
import { FakeData } from '../fake-data';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  news = FakeData.news;

  constructor() { }

  ngOnInit() {
  }

}
