import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FakeData } from '../../fake-data';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent implements OnInit {

  item: any;
  id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  moreNews = FakeData.news.articles.slice(0, 5);

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.item = FakeData.news.articles.filter(item => item.id === this.id)[0];
  }

}
