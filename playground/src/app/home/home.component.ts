import { Component, OnInit } from '@angular/core';
import { FakeData } from '../fake-data';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  news = FakeData.news.articles.slice(0, 5);
  users: any[];

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }
  getUsers(): void {
    this.usersService.getUsers().subscribe(
      (response) => {
        this.users = response.slice(0, 6);
      });
  }

}
