import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson4-sliding-sidebar',
  templateUrl: './lesson4-sliding-sidebar.component.html',
  styleUrls: ['./lesson4-sliding-sidebar.component.css']
})
export class Lesson4SlidingSidebarComponent implements OnInit {

  menu = true;

  constructor() { }

  ngOnInit() {
  }
  toggleMenu() {
    this.menu = !this.menu;
  }

}
