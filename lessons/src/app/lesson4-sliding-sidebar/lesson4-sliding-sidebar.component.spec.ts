import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson4SlidingSidebarComponent } from './lesson4-sliding-sidebar.component';

describe('Lesson4SlidingSidebarComponent', () => {
  let component: Lesson4SlidingSidebarComponent;
  let fixture: ComponentFixture<Lesson4SlidingSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson4SlidingSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson4SlidingSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
