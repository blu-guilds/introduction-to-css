import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson5SassStructureComponent } from './lesson5-sass-structure.component';

describe('Lesson5SassStructureComponent', () => {
  let component: Lesson5SassStructureComponent;
  let fixture: ComponentFixture<Lesson5SassStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson5SassStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson5SassStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
