import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson5-sass-structure',
  templateUrl: './lesson5-sass-structure.component.html',
  styleUrls: ['./lesson5-sass-structure.component.scss']
})
export class Lesson5SassStructureComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
