import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit, OnDestroy {

  showAlert = true;
  scrollEvent = false;

  constructor() { }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }
  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }
  scroll = (): void => {
    this.scrollEvent = window.scrollY > 200 ? true : false;
  }
  closeAlert = (): void => {
    this.showAlert = false;
  }

}
