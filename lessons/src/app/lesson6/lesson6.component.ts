import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson6',
  templateUrl: './lesson6.component.html',
  styleUrls: ['./lesson6.component.scss']
})
export class Lesson6Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  tilesBackground(): void {
    document.body.classList.remove('clouds');
    document.body.classList.add('tiles');
  }
  cloudsBackground(): void {
    document.body.classList.remove('tiles');
    document.body.classList.add('clouds');
  }
  noBackground() {
    document.body.classList.remove('tiles');
    document.body.classList.remove('clouds');
  }

}
