import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Blubito CSS Lessons';
  header = true;
  constructor(private router: Router) {
    router.events.subscribe(() => {
      this.header = (router.url.split('/').length > 2 || router.url === '/playground') ? false : true;
    });
  }
}
