import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson4FlexComponent } from './lesson4-flex.component';

describe('Lesson4FlexComponent', () => {
  let component: Lesson4FlexComponent;
  let fixture: ComponentFixture<Lesson4FlexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson4FlexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson4FlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
