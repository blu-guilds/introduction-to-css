import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson4SquaresGridComponent } from './lesson4-squares-grid.component';

describe('Lesson4SquaresGridComponent', () => {
  let component: Lesson4SquaresGridComponent;
  let fixture: ComponentFixture<Lesson4SquaresGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson4SquaresGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson4SquaresGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
