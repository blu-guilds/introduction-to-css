import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson3LayoutComponent } from './lesson3-layout.component';

describe('Lesson3LayoutComponent', () => {
  let component: Lesson3LayoutComponent;
  let fixture: ComponentFixture<Lesson3LayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson3LayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson3LayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
