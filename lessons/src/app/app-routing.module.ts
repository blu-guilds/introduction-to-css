import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { PlaygroundComponent } from './playground/playground.component';
import { Lesson1Component } from './lesson1/lesson1.component';
import { Lesson2Component } from './lesson2/lesson2.component';
import { Lesson3Component } from './lesson3/lesson3.component';
import { Lesson3LayoutComponent } from './lesson3-layout/lesson3-layout.component';
import { Lesson4Component } from './lesson4/lesson4.component';
import { Lesson4SquaresGridComponent } from './lesson4-squares-grid/lesson4-squares-grid.component';
import { Lesson4StickyHeaderComponent } from './lesson4-sticky-header/lesson4-sticky-header.component';
import { Lesson4SlidingSidebarComponent } from './lesson4-sliding-sidebar/lesson4-sliding-sidebar.component';
import { Lesson4FixedWidthComponent } from './lesson4-fixed-width/lesson4-fixed-width.component';
import { Lesson4FlexComponent } from './lesson4-flex/lesson4-flex.component';
import { Lesson5Component } from './lesson5/lesson5.component';
import { Lesson5SassStructureComponent } from './lesson5-sass-structure/lesson5-sass-structure.component';
import { Lesson6Component } from './lesson6/lesson6.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'playground', component: PlaygroundComponent },
  { path: 'lesson1', component: Lesson1Component },
  { path: 'lesson2', component: Lesson2Component },
  { path: 'lesson3', component: Lesson3Component },
  { path: 'lesson4', component: Lesson4Component },
  { path: 'lesson5', component: Lesson5Component },
  { path: 'lesson6', component: Lesson6Component },
  { path: 'lesson3/layout', component: Lesson3LayoutComponent },
  { path: 'lesson4/squares-grid', component: Lesson4SquaresGridComponent },
  { path: 'lesson4/sticky-header', component: Lesson4StickyHeaderComponent },
  { path: 'lesson4/sliding-sidebar', component: Lesson4SlidingSidebarComponent },
  { path: 'lesson4/fixed-width', component: Lesson4FixedWidthComponent },
  { path: 'lesson4/flex', component: Lesson4FlexComponent },
  { path: 'lesson5/sass-structure', component: Lesson5SassStructureComponent },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
