import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Lesson1Component } from './lesson1/lesson1.component';
import { PlaygroundComponent } from './playground/playground.component';
import { AppRoutingModule } from './app-routing.module';
import { Lesson2Component } from './lesson2/lesson2.component';
import { Lesson3Component } from './lesson3/lesson3.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Lesson3LayoutComponent } from './lesson3-layout/lesson3-layout.component';
import { Lesson4Component } from './lesson4/lesson4.component';
import { Lesson4SquaresGridComponent } from './lesson4-squares-grid/lesson4-squares-grid.component';
import { Lesson4StickyHeaderComponent } from './lesson4-sticky-header/lesson4-sticky-header.component';
import { Lesson4SlidingSidebarComponent } from './lesson4-sliding-sidebar/lesson4-sliding-sidebar.component';
import { Lesson4FixedWidthComponent } from './lesson4-fixed-width/lesson4-fixed-width.component';
import { Lesson4FlexComponent } from './lesson4-flex/lesson4-flex.component';
import { Lesson5Component } from './lesson5/lesson5.component';
import { Lesson6Component } from './lesson6/lesson6.component';
import { Lesson5SassStructureComponent } from './lesson5-sass-structure/lesson5-sass-structure.component';

@NgModule({
  declarations: [
    AppComponent,
    Lesson1Component,
    PlaygroundComponent,
    Lesson2Component,
    Lesson3Component,
    WelcomeComponent,
    Lesson3LayoutComponent,
    Lesson4Component,
    Lesson4SquaresGridComponent,
    Lesson4StickyHeaderComponent,
    Lesson4SlidingSidebarComponent,
    Lesson4FixedWidthComponent,
    Lesson4FlexComponent,
    Lesson5Component,
    Lesson6Component,
    Lesson5SassStructureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
