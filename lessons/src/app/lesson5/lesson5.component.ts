import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson5',
  templateUrl: './lesson5.component.html',
  styleUrls: ['./lesson5.component.scss']
})
export class Lesson5Component implements OnInit {

  doNotCode = '<a><img src="assests/images/icons/home.png"> Home</a>';
  doNotFA = '<a><i class="fa fa-home"></i> Home</a>';
  doNotFA2 = '<button>Save <i class="fa fa-save"></i></button>';

  constructor() { }

  ngOnInit() {
  }

}
