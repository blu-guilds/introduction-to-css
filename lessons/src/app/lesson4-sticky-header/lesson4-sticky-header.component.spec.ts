import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson4StickyHeaderComponent } from './lesson4-sticky-header.component';

describe('Lesson4StickyHeaderComponent', () => {
  let component: Lesson4StickyHeaderComponent;
  let fixture: ComponentFixture<Lesson4StickyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson4StickyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson4StickyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
