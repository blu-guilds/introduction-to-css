import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-lesson4-sticky-header',
  templateUrl: './lesson4-sticky-header.component.html',
  styleUrls: ['./lesson4-sticky-header.component.css']
})
export class Lesson4StickyHeaderComponent implements OnInit, OnDestroy {

  scrollEvent = false;

  constructor() { }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }
  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }
  scroll = (): void => {
    this.scrollEvent = window.scrollY > 200 ? true : false;
  }

}
