import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lesson4FixedWidthComponent } from './lesson4-fixed-width.component';

describe('Lesson4FixedWidthComponent', () => {
  let component: Lesson4FixedWidthComponent;
  let fixture: ComponentFixture<Lesson4FixedWidthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lesson4FixedWidthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lesson4FixedWidthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
