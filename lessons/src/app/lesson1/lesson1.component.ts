import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson1',
  templateUrl: './lesson1.component.html',
  styleUrls: ['./lesson1.component.css']
})
export class Lesson1Component implements OnInit {

  listExample = [
    { name: 'First item', active: false },
    { name: 'Another item', active: false },
    { name: 'Yet another item', active: true },
    { name: 'oh, even one more item', active: false }
  ];

  constructor() { }

  ngOnInit() {
  }

}
