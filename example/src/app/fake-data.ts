export class FakeData {
    public static news = {
        status: 'ok',
        totalResults: 10,
        articles: [
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 1,
                author: null,
                title: 'Google ist nicht die beste Suchmaschine',
                description: 'Die Stiftung Warentest bescheinigt Google technische Überlegenheit, kritisiert aber die Datensammelei des Internetkonzerns. Was ist die beste Alternative?',
                url: 'https://www.tagesspiegel.de/wirtschaft/analyse-von-stiftung-warentest-google-ist-nicht-die-beste-suchmaschine/24145554.html',
                urlToImage: 'https://www.tagesspiegel.de/images/google-suche/24145598/1-format530.jpg',
                publishedAt: '2019-03-26T11:02:41+00:00',
                content: 'Die besten Suchergebnisse liefert Google und der Nutzungskomfort stimmt. Daran lässt auch die Stiftung Warentest keinen Zweifel, die zehn Suchmaschinen unter die Lupe genommen hat (test-Ausgabe 4/19).\r\nAllerdings stellten die Tester sehr deutliche Mängel in d… [+1298 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 2,
                author: 'Simon Rayß',
                title: 'Das Herz des Bären',
                description: 'Der syrische Künstler Ammar al-Beik ist weltweit gefragt. Seit 2014 lebt er in Berlin. Das Haus am Waldsee zeigt nun seine Werkschau „One to free“.',
                url: 'https://www.tagesspiegel.de/kultur/ammar-al-beik-im-portraet-das-herz-des-baeren/24142976.html',
                urlToImage: 'https://www.tagesspiegel.de/images/heprodimagesfotos86120190326syrien_1073_1_20190325130528942-jpg/24143000/2-format530.jpg',
                publishedAt: '2019-03-26T10:56:35+00:00',
                content: '2011 gibt es kein Zurück mehr. Ammar al-Beik muss seine Heimat Damaskus verlassen. Zu unverhohlen ist die Kritik an Assads Regime, die er in seinen Filmen geübt hat. Über Beirut und Dubai flieht er 2014 nach Berlin. Acht Monate Aufenthalt in einem Containerla… [+4768 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 3,
                author: 'Claus Vetter',
                title: 'Schwere Knieverletzung: Play-off-Aus für Münchner Konrad Abeltshauser',
                description: 'Die Eisbären spielen in München stark auf und siegen 3:0. Ein Münchner verletzt sich dabei folgenschwer und muss operiert werden. Mehr im Blog.',
                url: 'https://www.tagesspiegel.de/sport/liveblog/eisbaeren-berlin-in-der-eishockey-saison-2018-19-schwere-knieverletzung-play-off-aus-fuer-muenchner-konrad-abeltshauser/14539956.html',
                urlToImage: 'https://www.tagesspiegel.de/images/ehc-red-bull-muenchen-eisbaeren-berlin/24145542/1-format530.jpg',
                publishedAt: '2019-03-26T10:37:34+00:00',
                content: '+++ Die Eisbären machen es noch mal und siegen in München +++ Entscheidend für das Play-off-Comeback war die Einstellung beider Teams +++ 2019 NHL Global Series Challenge am 29. September in Berlin +++ Kölns Kapitän Moritz Müller ist von den Eisbären überrasc… [+3454 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 4,
                author: null,
                title: 'Nach neuer Gaza-Gewalt herrscht vorerst Ruhe',
                description: 'Der Konflikt zwischen Israel und der Hamas hat sich seit Montag hochgeschaukelt. Nun bleibt es ruhig, doch von Waffenruhe will Israel nichts wissen.',
                url: 'https://www.tagesspiegel.de/politik/nahost-konflikt-nach-neuer-gaza-gewalt-herrscht-vorerst-ruhe/24145494.html',
                urlToImage: 'https://www.tagesspiegel.de/images/konflikt-in-gaza/24145520/1-format530.jpg',
                publishedAt: '2019-03-26T10:33:13+00:00',
                content: 'Nach Raketenangriffen aus dem Gazastreifen in der Nacht ist es am Dienstag im israelischen Grenzgebiet bis Mittag weitgehend ruhig geblieben. Es gab auch keine Berichte über neue israelische Luftangriffe in dem Palästinensergebiet. Minister von der konservati… [+1478 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 5,
                author: null,
                title: 'Deutsche Wohnen steigert Gewinn mit höheren Mieten',
                description: 'Der Immobilienkonzern hat seine Bilanz vorgelegt. Im laufenden Jahr will das wegen drastischen Mieterhöhungen in der Kritik stehende Unternehmen noch zulegen.',
                url: 'https://www.tagesspiegel.de/berlin/immobilienkonzern-deutsche-wohnen-steigert-gewinn-mit-hoeheren-mieten/24144784.html',
                urlToImage: 'https://www.tagesspiegel.de/images/deutsche-wohnen/24144948/1-format530.jpg',
                publishedAt: '2019-03-26T10:32:12+00:00',
                content: 'Steigende Mieten vor allem in Berlin haben dem Immobilienkonzern Deutsche Wohnen zu deutlich mehr Gewinn verholfen. 2018 legte der operative Gewinn im Jahresvergleich um elf Prozent auf knapp 480 Millionen Euro zu, wie das MDax-Unternehmen am Dienstag in Berl… [+1701 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 6,
                author: 'Hannes Heine',
                title: 'Medizinmann Michael Müller',
                description: 'Der Regierende Bürgermeister hat bei der Gesundheitsstadt-Planung ausnahmsweise ein gutes Händchen bewiesen: Er setzt auf die richtigen Köpfe. Ein Kommentar.',
                url: 'https://www.tagesspiegel.de/berlin/berlin-als-gesundheitsmetropole-medizinmann-michael-mueller/24143600.html',
                urlToImage: 'https://www.tagesspiegel.de/images/pk-zu-zweieinhalb-jahren-rot-rot-gruen-in-berlin/24143758/3-format530.jpg',
                publishedAt: '2019-03-26T10:30:53+00:00',
                content: 'Die Berliner Sozialdemokraten wirken während sie den Regierenden Bürgermeister stellen ähnlich glücklos wie die Bundespartei. Die von der Berliner SPD geführte rot-rot-grüne Koalition kommt bei Lehrermangel, Mietenirrsinn und Nahverkehrsüberlastung kaum voran… [+2614 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 7,
                author: 'Ulrich Zawatka-Gerlach',
                title: 'Hertha BSC soll jetzt doch im Olympiastadion bleiben',
                description: 'Die SPD will Hertha unbedingt in Charlottenburg halten – "erste Option" sei das Olympiastadion. Bei einem Neubau ist die SPD streng.',
                url: 'https://www.tagesspiegel.de/berlin/landes-spd-gegen-neubau-hertha-bsc-soll-jetzt-doch-im-olympiastadion-bleiben/24145456.html',
                urlToImage: 'https://www.tagesspiegel.de/images/hertha10/22580170/2-format530.jpg',
                publishedAt: '2019-03-26T10:30:22+00:00',
                content: 'Die Berliner SPD will Hertha BSC über 2025 hinaus im Olympiastadion halten. Dies bleibe für uns erste Option, steht in einem Antrag für den Landesparteitag der Sozialdemokraten am 30. März, der mit Sicherheit beschlossen wird. \r\nDie Antragskommission des SPD-… [+3634 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 8,
                author: null,
                title: 'Polizei-Taucher suchen im Herzberger See',
                description: 'Die Polizei hat am Montag ihre Suche nach der vermissten Rebecca Reusch in Ost-Brandenburg fortgesetzt. Zudem hat sie die Bilder des Schwagers gelöscht.',
                url: 'https://www.tagesspiegel.de/berlin/rebecca-reusch-polizei-taucher-suchen-im-herzberger-see/24142676.html',
                urlToImage: 'https://www.tagesspiegel.de/images/weitere-suche-nach-rebecca/24142770/1-format530.jpg',
                publishedAt: '2019-03-26T10:23:54+00:00',
                content: 'Fünf Wochen nach dem Verschwinden der 15-jährigen Rebecca Reusch aus Berlin hat die Polizei eine Suche an einem kleinen See in Ost-Brandenburg gestartet. Mit einem Boot und Echolot suchte die Polizei den Herzberger See ab. \r\nAuch ein Taucher war am Ufer und i… [+2229 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 9,
                author: null,
                title: 'Für Ziele und Tore: Mit der Fußballakademie nach Südafrika',
                description: 'Bernd Steinhage spielte für TeBe und TuS Makkabi, nun leitet er eine Fußballakademie in Südafrika. Die vereint Kinder unterschiedlicher Verhältnisse.',
                url: 'https://www.tagesspiegel.de/sport/young-bafana-soccer-academy-fuer-ziele-und-tore-mit-der-fussballakademie-nach-suedafrika/24139300.html',
                urlToImage: 'https://www.tagesspiegel.de/images/heprodimagesfotos85120190325bafanatrain_4935_1_20190324143247143-jpg/24139336/3-format530.jpg',
                publishedAt: '2019-03-26T10:22:21+00:00',
                content: 'Goal, das ist im Englischen ein Wort mit doppelter Bedeutung. Es steht zum einen für das Ziel und  davon abgeleitet zugleich auch für das Tor. Bernd Steinhage spielt mit dieser Ambiguität: Der gebürtige Südafrikaner, Sohn einer Namibianerin und eines Deutsche… [+5739 chars]'
            },
            {
                source: {
                    id: 'der-tagesspiegel',
                    name: 'Der Tagesspiegel'
                },
                id: 10,
                author: null,
                title: 'Mehr Schüler fallen durchs Abitur',
                description: 'Der Anteil der Schülerinnen und Schüler in Deutschland, die das Abitur nicht bestehen, steigt stetig. In Berlin ist die Quote überdurchschnittlich hoch.',
                url: 'https://www.tagesspiegel.de/wissen/bundesweite-quote-der-durchfaller-mehr-schueler-fallen-durchs-abitur/24145382.html',
                urlToImage: 'https://www.tagesspiegel.de/images/immer-mehr-schueler-fallen-durchs-abitur/24145484/1-format530.jpg',
                publishedAt: '2019-03-26T10:20:12+00:00',
                content: 'Die wichtigsten Prüfungen der Oberstufe enden für immer mehr Jugendliche enttäuschend: In Deutschland fallen wieder mehr Schüler durchs Abitur. Seit 2009 ist die Quote der nicht bestandenen Prüfungen nahezu stetig gestiegen, wie eine Auswertung der Nachrichte… [+3918 chars]'
            }
        ]
    };
}